Ansible Role: build-packer
=========

This role was written to allow me to build the docker containers I use for testing Ansible in AWS rather than my local laptop.  It does not create the AWS instance.  It just installs Packer and Docker and then runs the packer builds that are included with it.  I generally use it in combonation with my [Vagrant Testing Suite](https://github.com/AustinCloudGuru/vagrant-ansible-testing).

It could certainly be modified to create many more types of containers.  

Requirements
------------

None.

Role Variables
--------------

The role takes your docker credentials as variables so that they are not stored in repo. You can also set the docker_tag variable to the container version.

     docker_username
     docker_email
     docker_password
     docker_tag

Dependencies
------------

None

Example Playbook
----------------

     - hosts: all
       become: true
       vars:
         docker_username: bob
         docker_email: bob@example.com
         docker_password: "bobspassword"
         docker_tag: "0.1"
       roles:
         - austincloudguru.build-packer

License
-------

MIT

Author Information
------------------

Mark Honomichl aka [AustinCloudGuru](https://austincloud.guru)
Created in 2016
